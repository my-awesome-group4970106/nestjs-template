import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Variables } from './setup/variables/variables.service';
import setupSwagger from './setup/swagger/configuration';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  setupSwagger(app);

  const variableService = app.get(Variables);

  const port = variableService.service.port;

  app.enableCors();

  await app.listen(port);
}
bootstrap();
