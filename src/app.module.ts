import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import setupConfiguration from './setup/configuration/configuration';
import { DatabaseModule } from './setup/database/database.module';
import { VariablesModule } from './setup/variables/variables.module';
import { HelloWorldModule } from './hello-world/hello-world.module';

@Module({
  imports: [
    ConfigModule.forRoot(setupConfiguration()),
    VariablesModule,
    DatabaseModule,
    HelloWorldModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
