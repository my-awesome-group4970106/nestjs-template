import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { Environment } from '../configuration/environment';
import { VariablesModule } from '../variables/variables.module';
import { Variables } from '../variables/variables.service';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [VariablesModule],
      inject: [Variables],
      useFactory: (variables: Variables) => {
        const isDevelopmentMode =
          variables.environment.environment === Environment.Development;
        return {
          type: 'postgres',
          ...variables.db,
          entities: [join(__dirname, '**', '*.entity.{ts,js}')],
          migrations: [join(__dirname, 'migrations', '**', '*.{ts,js}')],
          synchronize: isDevelopmentMode,
          autoLoadEntities: true,
        };
      },
    }),
  ],
})
export class DatabaseModule {}
