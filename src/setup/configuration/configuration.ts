import { ConfigModuleOptions } from '@nestjs/config';
import validate from './validation';

function setupVariables(): GlobalVariables.Variables {
  const variables = process.env;
  return {
    service: {
      port: parseInt(variables.PORT),
    },
    db: {
      port: parseInt(variables.DB_PORT),
      host: variables.DB_HOST,
      username: variables.DB_USER,
      password: variables.DB_PASSWORD,
      database: variables.DB_NAME,
    },
    environment: {
      environment: variables.NODE_ENV,
    },
    jwt: {
      secret: variables.JWT_SECRET,
    },
  };
}

function setupEnvFile() {
  const variables = process.env;
  if (!variables.NODE_ENV) {
    return `.env`;
  }
  return `.env.${variables.NODE_ENV}`;
}

function setupConfiguration(): ConfigModuleOptions {
  return {
    envFilePath: setupEnvFile(),
    load: [setupVariables],
    validate: validate,
  };
}

export default setupConfiguration;
